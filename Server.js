var WebSocketServer = require('ws').Server,
wss = new WebSocketServer({ port: 8080 }),
trees = require('./Trees'),
groundItems = require('./GroundItems'),
player = require('./player/Player'),
mapregion = require('./MapRegion'),
gameloop = require('./GameLoop'),
database = require('./Database')
loop = new gameloop.GameLoop(),
db = null,
config = require('./config.json'),
_ = require("underscore"),
msgpack = require('msgpack-js');


if(!_.isUndefined(config.database) && config.database) {
    db = new database.Database(config.databaseUrl, function dbReady(ref) {
        trees.init(ref);
    })
}else{
    console.log("[mongodb] Disabled");
    trees.init(null);
}

for (var x = 0; x < 100; x++){
    for(var y = 0; y < 100; y++){
        new mapregion.MapRegion(x,y);
    }
}

process.on('exit', function () {
    console.log('About to exit.');
    if(!_.isNull(db)){
        db.close();
    }
});

process.on('SIGINT', function () {
    console.log('Got SIGINT.');
    process.exit(0);
});

(function(){
    wss.on('connection', function connection(ws) {
        ws.on('close', function close() {
            //console.log('disconnected');
            player.removeWS(ws);
        });

        ws.on('message', function incoming(message) {
            var data = msgpack.decode(message);

            if(data.players){
                if(_.isArray(data.players)){
                    data.players.map(function addPlayers(name) {
                        if(_.isNull(player.get(name))){
                            ws.send(msgpack.encode(new player.Player(name,ws,loop,mapregion).toJSON()),{ binary: true});
                        }
                    });
                }
            }else{
                var _player = player.getForWS(ws);
                if(!_.isNull(_player)){
                    _player.packet(data);
                }
            }
        });

        ws.send(msgpack.encode({welcome: true, trees: trees.getTrees(true), groundItems: groundItems.items},{ binary: true }));
    });

})();