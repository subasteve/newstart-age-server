/**
 * Created by subasteve on 3/30/15.
 */
(function(){
    var self = {},
        world = require('./World'),
        _ = require("underscore"),
        util = require('./Util');

    self.trees = [];

    function createSomeTrees(){
        for(var i = 0; i < 150; i++){
            self.trees.push({
                id: i,
                object: [],
                removed: false,
                x: util.random(-world.width,world.width),
                y: util.random(-world.height,world.height)
            });
        }
    }

    self.init = function init(db){
        //if load database
        if(!_.isNull(db)){
            var collection = db.collection('Trees');
            // Fetch all results
            collection.find().toArray(function(error, items) {
                if(_.isNull(error)){
                    //load trees
                    if(items.length > 0){
                        self.trees = items;
                    }else{
                        //if no trees create some
                        createSomeTrees();
                        collection.insert(self.trees, {w:1}, function(error, result) {
                            if(!_.isNull(error)){
                                console.error(error);
                            }
                        });

                    }
                }
            });
        }else{
            createSomeTrees();
        }


    }


    self.getTrees = function getTrees(alive) {
        if(_.isUndefined(alive)){
            return self.trees;
        }else{
            return self.trees.filter(function filterTrees(tree) {
                if(tree.removed && !alive){
                    return true;
                }else if(!tree.removed && alive) {
                    return true;
                }
                return true;
            });
        }
        return self.trees;
    };


    self.getTree = function getTree(id) {
        return trees[id];
    };

    self.remove = function remove(id) {
        if(!_.isUndefined(self.trees[id])) {
            if (!self.trees[id].removed) {
                //TODO: update db model
                self.trees[id].removed = true;
                return self.trees[id];
            }
        }
        return null;
    };

    module.exports = self;
})();
