/**
 * Created by subasteve on 4/5/15.
 */

(function() {
    var self = {},
        MongoClient = require('mongodb').MongoClient,
        _ = require("underscore");

    self.Database = function Database(url,callback) {
            MongoClient.connect(url, _.bind(function (error, db) {
                if ((_.isUndefined(error) || _.isNull(error)) && _.isUndefined(this.dbRef)) {
                    this.dbRef = db;
                    console.log("[mongodb] Connected");
                    callback(this.dbRef);
                }else{
                    console.error(error);
                }
            }, this));
    }

    self.Database.prototype.ref = function ref() {
        return this.dbRef;
    };

    self.Database.prototype.close = function close(){
        if(this.dbRef !== null){
            this.dbRef.close();
            this.dbRef = null;
            console.log("[mongodb] Closed");
        }
    };

    module.exports = self;
})();