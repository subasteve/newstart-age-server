/**
 * Created by subasteve on 3/31/15.
 */
(function(){
    var self = {};

    self.Equipment = function Equipment(id) {
        this.helment = -1;
        this.body = -1;
        this.legs = -1;
        this.hand1 = -1;
        this.hand2 = -1;
        this.cape = -1;
        this.ring = -1;
        this.amulet = -1;
    };

    self.Equipment.prototype.setHelment = function setHelment(id){
        this.helment = id;
    };

    self.Equipment.prototype.getHelment = function getHelment(){
        return this.helment;
    };

    self.Equipment.prototype.setBody = function setBody(id){
        this.body = id;
    };

    self.Equipment.prototype.getBody = function getBody(){
        return this.body;
    };

    self.Equipment.prototype.setLegs = function setLegs(id){
        this.legs = id;
    };

    self.Equipment.prototype.getLegs = function getLegs(){
        return this.legs;
    };

    self.Equipment.prototype.setHand1 = function setHand1(id){
        this.hand1 = id;
    };

    self.Equipment.prototype.getHand1 = function getHand1(){
        return this.hand1;
    };

    self.Equipment.prototype.setHand2 = function setHand2(id){
        this.hand1 = id;
    };

    self.Equipment.prototype.getHand2 = function getHand2(){
        return this.hand1;
    };

    self.Equipment.prototype.setCape = function setCape(id){
        this.cape = id;
    };

    self.Equipment.prototype.getCape = function getCape(){
        return this.cape;
    };

    self.Equipment.prototype.setCape = function setCape(id){
        this.cape = id;
    };

    self.Equipment.prototype.getCape = function getCape(){
        return this.cape;
    };

    self.Equipment.prototype.setRing = function setRing(id){
        this.ring = id;
    };

    self.Equipment.prototype.getRing = function getRing(){
        return this.ring;
    };

    self.Equipment.prototype.setAmulet = function setAmulet(id){
        this.amulet = id;
    };

    self.Equipment.prototype.getAmulet = function getAmulet(){
        return this.amulet;
    };

    self.Equipment.prototype.toJSON = function toJSON() {
        return {
            helment: this.helment,
            body: this.body,
            legs: this.legs,
            hand1: this.hand1,
            hand2: this.hand2,
            cape: this.cape,
            ring: this.ring,
            amulet: this.amulet
        };
    };

    module.exports = self;
})();
