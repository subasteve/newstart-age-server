/**
 * Created by subasteve on 3/30/15.
 */
(function(){
    var self = {},
        _ = require("underscore"),
        trees = require('../Trees'),
        groundItems = require('../GroundItems'),
        equipment = require('./Equipment'),
        msgpack = require('msgpack-js');

    self.Player = function Player(name,ws,loop,mapregion) {
        this.centerTile = {x: 0, y: 0};
        this.newSheet = null;
        this.oldSheet = null;
        this.position = {
            x: 0,
            y: 0,
            z: 0
        };
        this.keys = {
            left: false,
            right: false,
            up: false,
            down: false
        };
        this.id = ws._ultron.id;
        this.ws = ws;
        this.name = name;
        this.equipment = new equipment.Equipment();

        this.send = function send(obj){
          this.ws.send(msgpack.encode(obj),{ binary: true, mask: true });
        };
        function onLoop() {
            var dx = 0,
                dy = 0,
                speed = 2,
                angle = 0,
                moved = false;
            if (this.keys.down && this.keys.right) {
                dx = speed;
                angle = 90;
            } else if (this.keys.up && this.keys.right) {
                dy = -speed;
                angle = 180;
            } else if (this.keys.up && this.keys.left) {
                dx = -speed;
                angle = 270;
            } else if (this.keys.down && this.keys.left) {
                dy = speed;
                angle = 0;
            } else if (this.keys.up) {
                dy = -(speed / 2);
                dx = -(speed / 2);
                angle = 225;
            } else if (this.keys.down) {
                dy = (speed / 2);
                dx = (speed / 2);
                angle = 45;
            } else if (this.keys.right) {
                dy = -(speed / 2);
                dx = (speed / 2);
                angle = 135;
            } else if (this.keys.left) {
                dy = (speed / 2);
                dx = -(speed / 2);
                angle = 315;
            }
            if (dx !== 0 && dy !== 0) {
                this.position.x += dx;
                this.position.y += dy;
                this.send({MovePlayer: {id: this.id, angle: angle, x: this.position.x, y: this.position.y}});
                moved = true;
            } else if (dx !== 0) {
                this.position.x += dx;
                this.send({MovePlayer: {id: this.id, angle: angle, x: this.position.x}});
                moved = true;
            }else if (dy !== 0) {
                this.position.y += dy;
                this.send({MovePlayer: {id: this.id, angle: angle, y: this.position.y}});
                moved = true;
            }

            if(moved){
                var oldCenterTile = {
                    x:this.centerTile.x,
                    y:this.centerTile.y
                };
                this.centerTile.x = Math.round(this.position.x / mapregion.tileSize);
                this.centerTile.y = Math.round(this.position.y / mapregion.tileSize);

                if (this.centerTile.x !== oldCenterTile.x || this.centerTile.y !== oldCenterTile.y) {
                    if(_.isNull(this.newSheet)){
                        this.newSheet = this.centerTile;
                        this.oldSheet = oldCenterTile;
                    }
                }

                if(!_.isNull(this.oldSheet)){
                    if (2 <= Math.abs(this.centerTile.x - this.oldSheet.x) || 2 <= Math.abs(this.centerTile.y - this.oldSheet.y)) {
                        this.send({UpdateMap: {oldSheet: this.oldSheet, newSheet: this.newSheet}});
                        this.oldSheet = null;
                        this.newSheet = null;
                    }
                }
            }
        }

        loop.events.on('process', _.bind(onLoop,this));
        self.players[ws._ultron.id] = this;
        self.playersNameIndex[name] = ws._ultron.id;
    };

    self.players = [];
    self.playersNameIndex = [];

    self.get = function get(name) {
        if(!_.isUndefined(self.playersNameIndex[name])) {
            return self.players[self.playersNameIndex[name]];
        }
        return null;
    };

    self.getForWS = function getForWS(ws) {
        if(!_.isUndefined(self.players[ws._ultron.id])){
            return self.players[ws._ultron.id];
        }
        return null;
    };

    self.removeWS = function removeWS(ws){
        if(!_.isUndefined(self.players[ws._ultron.id])){
            delete self.playersNameIndex[self.players[ws._ultron.id].name];
            self.players.splice(ws._ultron.id, 1);
        }
    }

    self.Player.prototype.getPosition = function getPosition() {
        return this.position;
    };

    self.Player.prototype.packet = function packet(data){
        if(data.chopTree){
            var tree = trees.remove(data.chopTree);
            if(!_.isNull(tree)){
                var item = {
                    id: 1511,
                    amount: 1,
                    x: tree.x,
                    y: tree.y
                };
                groundItems.addItem(item);
                this.send({removeTree: data.chopTree, createGroundItem: item});
            }
        }
        if(data.Player) {
            if(data.Player.keys) {
                if (!_.isUndefined(data.Player.keys.l)) {
                    this.keys.left = data.Player.keys.l;
                }
                if (!_.isUndefined(data.Player.keys.r)) {
                    this.keys.right = data.Player.keys.r;
                }
                if (!_.isUndefined(data.Player.keys.u)) {
                    this.keys.up = data.Player.keys.u;
                }
                if (!_.isUndefined(data.Player.keys.d)) {
                    this.keys.down = data.Player.keys.d;
                }
            }
        }
    };

    self.Player.prototype.toJSON = function toJSON(data) {
        return {Player: {
            Equipment: this.equipment.toJSON(),
            Position: this.position,
            name: this.name
        }};
    };

    module.exports = self;
})();
