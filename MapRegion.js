/**
 * Created by subasteve on 4/2/15.
 */
(function(){
    var self = {},
        _ = require("underscore"),
        trees = require('./Trees'),
        groundItems = require('./GroundItems');


    self.tileSize = 400;
    self.orgin = {x: 0, y: 0}

    self.MapRegion = function MapRegion(x,y) {
        this.position = {
            x: x,
            y: y,
            id: x << 8 | y
        };
        var idX = this.position.id >> 8 & 0xFF,
            idY = this.position.id & 0xFF;

        if (idX !== x || idY !== y) {
            console.error(x, y, idX, idY);
        }
        self.regions[this.position.id] = this;
    };

    self.regions = [];

    module.exports = self;
})();
