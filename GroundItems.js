/**
 * Created by subasteve on 3/30/15.
 */
(function(){
    var self = {};

    self.items = [];

    self.addItem = function addItem(item) {
        self.items.push({id: item.id, amount: item.amount, x: item.x, y: item.y});
    };

    module.exports = self;
})();
