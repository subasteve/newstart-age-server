/**
 * Created by subasteve on 3/30/15.
 */
(function(){
    var self = {};

    self.random = function random(min,max){
        return Math.floor(Math.random() * max) + min;
    };

    module.exports = self;
})();

