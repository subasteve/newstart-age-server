NewStart Age Server
========================================
Prototype of server done in nodejs since it is easier to communicate to browser js <-> js. Client located [Newstart Age Client](https://bitbucket.org/subasteve/newstart-age-client).

Prerequisites
-------------
1. Node.js
2. npm

Run Server
-------------
```shell
nodejs Server.js
```

Install Packages
-------------
```shell
npm install
```
