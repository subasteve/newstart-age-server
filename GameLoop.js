/**
 * Created by subasteve on 4/2/15.
 */
(function(){
    var self = {},
        events = require('events'),
        eventEmitter = new events.EventEmitter();

    self.GameLoop = function GameLoop() {
        setInterval(function loop(){
            eventEmitter.emit('process');
        }, 30);
    };

    self.GameLoop.prototype.events = eventEmitter;

    module.exports = self;
})();